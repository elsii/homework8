<?php

namespace task11\classes;

class ContactBuilder {
    public function __construct()
    {
        $this->contact = new Contact();
    }

    public function name($name)
    {
        $this->contact->setName($name);
        return $this;
    }

    public function surname($surname)
    {
        $this->contact->setSurname($surname);
        return $this;
    }

    public function email($email)
    {
        $this->contact->setEmail($email);
        return $this;
    }

    public function phone($phone)
    {
        $this->contact->setPhone($phone);
        return $this;
    }

    public function address($address)
    {
        $this->contact->setAddress($address);
        return $this;
    }

    public function build()
    {
        return $this->contact;
    }

    public function reset()
    {
        $this->contact = new Contact();
    }
}