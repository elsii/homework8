<?php

require_once '../vendor/autoload.php';

use task11\classes\ContactBuilder;

$contact = new ContactBuilder();
$newContact = $contact->phone('000-555-000')
    ->name("John")
    ->surname("Surname")
    ->email("john@email.com")
    ->address("Some address")
    ->build();

print "<pre style='color: red;'>";
print_r($newContact);
die();
print "</pre>";

