<?php

namespace task10\IntFace;

interface TypeDelivery
{
    public function getCarModel();
    public function getCost();
}