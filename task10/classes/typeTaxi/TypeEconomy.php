<?php

namespace task10\classes\typeTaxi;

use task10\IntFace\TypeDelivery;

class TypeEconomy implements TypeDelivery
{
    public function getCarModel()
    {
        print "car model economy";
    }

    public function getCost()
    {
        print "price economy";
    }
}