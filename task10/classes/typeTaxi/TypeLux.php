<?php

namespace task10\classes\typeTaxi;

use task10\IntFace\TypeDelivery;

class TypeLux implements TypeDelivery
{
    public function getCarModel()
    {
        print "car model lux";
    }

    public function getCost()
    {
        print "price lux";
    }
}