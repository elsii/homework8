<?php

namespace task10\classes\typeTaxi;

use task10\IntFace\TypeDelivery;

class TypeStandard implements TypeDelivery
{
    public function getCarModel()
    {
        print "car model standard";
    }

    public function getCost()
    {
        print "price standard";
    }
}