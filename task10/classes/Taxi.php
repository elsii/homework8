<?php

namespace task10\classes;

use task10\IntFace\TypeDelivery;

class Taxi
{
    protected $typeDelivery;

    public function __construct(TypeDelivery $typeDelivery)
    {
        $this->typeDelivery = $typeDelivery;
    }

    public function getCarModel()
    {
        return $this->typeDelivery->getCarModel();
    }

    public function getCost()
    {
        return $this->typeDelivery->getCost();
    }

}