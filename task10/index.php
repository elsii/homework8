<?php

require_once '../vendor/autoload.php';

use task10\classes\Taxi;
use task10\classes\typeTaxi\TypeEconomy;
use task10\classes\typeTaxi\TypeStandard;
use task10\classes\typeTaxi\TypeLux;

$economy = new Taxi(new TypeEconomy());
$economy->getCarModel();
print "<br />------<br />";
$economy->getCost();
print "<br />======== <br />";
$economy = new Taxi(new TypeStandard());
$economy->getCarModel();
print "<br />------<br />";
$economy->getCost();
print "<br />======== <br />";
$economy = new Taxi(new TypeLux());
$economy->getCarModel();
print "<br />------<br />";
$economy->getCost();

