<?php

require_once '../vendor/autoload.php';

use task9\classes\Payment;
use task9\classes\allPayments\LiqPay;
use task9\classes\allPayments\WayForPay;

$liqPay = new Payment(new LiqPay());
$liqPay->pay([0,1,2,3]);
$liqPay->getTransaction('testId');
print "<br /> ========== <br />";
$wayForPay = new Payment(new WayForPay());
$wayForPay->pay([4,5,6,7]);
$wayForPay->getTransaction('testId-WayForPay');

