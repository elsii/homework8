<?php

namespace task9\IntFace;

interface PaymentSystem
{
    public function pay($params);
    public function getTransaction($idTransaction);
}