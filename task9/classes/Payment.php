<?php

namespace task9\classes;

use task9\IntFace\PaymentSystem;

class Payment {

    protected $paymentSystem;

    public function __construct(PaymentSystem $paymentSystem)
    {
        $this->paymentSystem = $paymentSystem;
    }

    /**
     * Function for pay
     *
     * @param $params
     * @return void
     */
    public function pay($params)
    {
        $this->paymentSystem->pay($params);
    }

    /**
     * Function for get transaction
     *
     * @param $idTransaction
     * @return void
     */
    public function getTransaction($idTransaction)
    {
        $this->paymentSystem->getTransaction($idTransaction);
    }
}