<?php

namespace task9\classes\allPayments;

use task9\IntFace\PaymentSystem;

class LiqPay implements PaymentSystem
{

    public function pay($params)
    {
        print "Function for pay <br />";
        print_r($params);
    }

    public function getTransaction($idTransaction)
    {
        print "<br /> get transaction info <br />";
        print_r($idTransaction);
    }
}